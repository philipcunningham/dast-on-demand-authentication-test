class PeopleController < InheritedResources::Base

  private

    def person_params
      params.require(:person).permit(:name)
    end

end
